#!/usr/bin/env node
var ConfigurationGroup = require('./ConfigurationGroup.js')
var downloader = module.exports

downloader.parseConfig = function(filename) {
  var textConfig = fs.readFileSync(filename);
  var configObject = JSON.parse(textConfig);

  var groups = [];

  if (!configObject.groups) {
    return groups;
  }

  var defaultOpts = configObject.default || null;

  for (configItem in configObject.groups) {
    var singleGroup = ConfigurationGroup.create(configItem, defaultOpts)
    if (singleGroup) {
      groups.push(singleGroup)
    }
  }

  return groups;
}

downloader.launchGroup = function