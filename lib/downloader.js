var logger = require('./logger')
var http = require('http')
var util = require('util');
var events = require('events');

var Downloader = module.exports = function(engines) {
  this.max_connections_ = 3
  this.delay_ = 0
  this.engines_ = engines

  this.currentEngine_ = null
  this.nextId_ = 0
  this.jobs_ = {}
  this.complete_ = false
}

util.inherits(Downloader, events.EventEmitter);

Downloader.prototype.setMaxConnections = function(value) {
  this.max_connections_ = value
}

Downloader.prototype.setDelay = function(value) {
  this.delay_ = value
}

Downloader.prototype.launch = function() {
  /*if (this.engines_.length == 0) {
    logger.log("No se indicaron sitios para descargar")
    process.exit(-1)
  }

  this.server_ = http.createServer(this.serverCallback.bind(this))
  this.server_.listen(8765, '0.0.0.0')*/

  for (var i = 0; i < this.max_connections_; i += 1) {
    this.launchNext_();
  }
};

Downloader.prototype.getNextId = function() {
  return this.nextId_ ++;
}

Downloader.prototype.createNextJob = function() {
  if (this.currentEngine_ == null) {
    this.currentEngine_ = 0
  }

  if (this.currentEngine_ >= this.engines_.length) {
    return false
  }

  var theEngine = this.engines_[this.currentEngine_]
  var nextJob = theEngine.next()

  if (nextJob === false) {
    this.currentEngine_ += 1
    return this.createNextJob()
  }

  var new_id = this.getNextId()

  nextJob.on('complete', this.onJobComplete_.bind(this, new_id))

  this.jobs_[new_id] = nextJob

  //nextJob.launch()

  return nextJob;
}

Downloader.prototype.serverCallback = function(req, res) {
  var text = "Memory: "
  var mem = process.memoryUsage()

  text += "\nRSS:        " + (mem.rss / 1024 / 1024).toString() + " MiB"
  text += "\nHeap Total: " + (mem.heapTotal / 1024 / 1024).toString() + " MiB"
  text += "\nHeap Used:  " + (mem.heapUsed / 1024 / 1024).toString() + " MiB"

  if (this.complete_) {
    text += "\n\nComplete"
  } else {
    text += "\n\nJobs: "

    for (id in this.jobs_) {
      text += "\n" + id + " - " + this.jobs_[id].url_
    }
  }

  res.end(text)
}

Downloader.prototype.onJobComplete_ = function(id, status) {
  this.jobs_[id].removeAllListeners('complete')

  delete this.jobs_[id]

  //logger.log("JobComplete: %d : %s", id, status)

  var applicableDelay = (status == 'downloaded' || status == 'not_found') ? this.delay_ : 0;

  this.launchNext_(applicableDelay)
}

Downloader.prototype.launchNext_ = function(opt_delay) {
  var delay = (opt_delay && opt_delay > 0) ? opt_delay : 0;

  var nextJob = this.createNextJob();

  if (nextJob===false) {

    if (this.isJobsEmpty()) {
      this.complete_ = true;
      this.emit('complete')
    }

    return
  }

  logger.log("Waiting %d ms", delay)
  //logger.log("NextJob: %s", nextJob)

  setTimeout(nextJob.launch.bind(nextJob), delay)
}

Downloader.prototype.isJobsEmpty = function() {
  for (var k in this.jobs_) {
    return false;
  }

  return true;
}
