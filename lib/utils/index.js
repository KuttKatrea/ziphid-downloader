#!/usr/bin/env node
var fs = require('fs')

utils = exports

utils.parseDecimalInt = function parseDecimalInt(str) {
  return parseInt(str, 10)
}

utils.parseIntRange = function parseIntRange(str) {
  var pieces = /((?:-|\+)?\d+):((?:-|\+)?\d+)/.exec(str)
  if ( ! pieces ) {
    throw new Error('Invalid format')
  }

  return {begin: parseInt(pieces[1], 10), end: parseInt(pieces[2], 10)}
}

utils.canWriteSync = function canWriteSync(path, uid, gid, stats) {
  stats = stats || fs.statSync(path)
  uid = uid || process.getuid()
  gid = gid || process.getgid()

  return (uid === stats.uid) && (stats.mode & parseInt('00200', 8)) || // User is owner and owner can write.
         (gid === stats.gid) && (stats.mode & parseInt('00020', 8)) || // User is in group and group can write.
         (stats.mode & parseInt('00002', 8)) // Anyone can write.
}

utils.isEmptyObject = function isEmptyObject(obj) {
  for (var k in obj) {
    return false;
  }

  return true;
}
