#!/usr/bin/env node
var program = require('commander')
var util = require('util')
var fs = require('fs')
var path = require('path')
var logger = require('./logger')

program.
  option('-c, --config [config_file]', 'Archivo de configuración', './zd.json').
  option('-v, --verbose', 'Nivel de verbosidad', false).
  option('-d, --debug', 'Modo de depuración', false).
  parse(process.argv)


// TODO: Validar
if (!program.config) {
  logger.log("Se requiere el parámetro --config")
  process.exit(-1)
}

if (!fs.existsSync(program.config)) {
  logger.log("El archivo de configuración %s no existe", program.config)
  process.exit(-1)
}

var configText = require('fs').readFileSync(program.config);

try {
  var configObject = JSON.parse(configText);
} catch (e) {
  logger.log("Formato del archivo de configuración [%s] inválido: \n%s\n",
    program.config, e);

  //logger.log(util.inspect(e))

  process.exit(-1);
}

//logger.log(util.inspect(configObject))

normalizeWorkingDir(configObject, path.dirname(program.config))

var siteConfigFile, single_profile, single_engine
var engines = []

if ( configObject.enabled_profiles ) {

  for (i=0,l=configObject.enabled_profiles.length;i<l;i+=1) {

    single_profile = configObject.enabled_profiles[i]

    if (typeof single_profile === 'string') {
      single_profile = loadProfile(single_profile, configObject.working_dir)
    } else if (typeof single_profile !== 'object') {
      logger.log("Los perfiles deben ser rutas a archivos json u objetos")
      process.exit(-1)
    } else {
      normalizeWorkingDir(single_profile.config, configObject.working_dir)
    }

    //logger.log("%j", single_profile)

    single_engine = require('./engine').create(single_profile)

    if (!single_engine) {
      logger.log("El engine especificado para el perfil %d es inválido", i);
      process.exit(-1);
    }

    engines.push(single_engine)
  }
}

var Downloader = require('./downloader')

var downloader = new Downloader(engines)
downloader.setMaxConnections(configObject.max_connections || 3)
downloader.setDelay(configObject.delay || 0)
downloader.launch()

downloader.on("complete", function() {
  logger.log("Process complete")
})

function loadProfile(config_file_path, working_dir) {
  config_file_path = path.resolve(working_dir, config_file_path)

  if (!fs.existsSync(config_file_path)) {
    //TODO: Exceptionize
    logger.log("El archivo de configuración %s no existe.", config_file_path)
    process.exit(-1)
  }

  try {
    var configFileContents = fs.readFileSync(config_file_path)
  } catch (e) {
    logger.log("Error leyendo el archivo %s: %s.", config_file_path, e)
    process.exit(-1)
    throw e;
  }

  try {
    var configObject = JSON.parse(configFileContents)
  } catch (e) {

    logger.log("Formato del archivo de configuración de sitio [%s] inválido: %s",
      config_file_path, e);

    process.exit(-1);
  }

  normalizeWorkingDir(configObject.config, path.dirname(config_file_path))

  return configObject
}

function normalizeWorkingDir(profile, global_dir) {
  if (!profile.working_dir) {
    profile.working_dir = global_dir
  } else {
    profile.working_dir = path.resolve(global_dir, profile.working_dir)
  }
}
