var util = require('util')
var engine_base = require('./base')

function clone(obj, opt_obj2) {
  var cloneObj = extend({}, obj);

  if (opt_obj2)
    extend(cloneObj, opt_obj2);

  return cloneObj;
}

function extend(obj1, obj2) {
  for (var k2 in obj2) {
    if (obj2.hasOwnProperty(k2)) {
      obj1[k2] = obj2[k2];
    }
  }

  return obj1;
}

function lpad(str, length) {
  str = '' + str;
  while (str.length < length)
      str = '0' + str;
  return str;
}

var HashedGameCDNEngine = function(config) {
  HashedGameCDNEngine.super_.apply(this, [config]);

  this.groups_ = {};
  this.pendingGroups_ = [];

  for (var k in config.groups) {
    var group = new HashedGameCDNGroup(this, k, clone(config.default || {}, config.groups[k]))
    this.groups_[k] = group;
    this.pendingGroups_.push(group);
  }

  this.nextGroup_ = 0;
};

util.inherits(HashedGameCDNEngine, engine_base.BaseEngine);

HashedGameCDNEngine.prototype.next = function() {
  if (this.isComplete()) {
    return false;
  }

  var group = this.pendingGroups_[this.nextGroup_];

  var next = group.next();

  if (group.isComplete()) {
    this.pendingGroups_.splice(this.nextGroup_, 1);
  } else {
    this.nextGroup_ += 1;
  }

  if (this.nextGroup_ >= this.pendingGroups_.length) {
    this.nextGroup_ = 0;
  }

  return next;
};

HashedGameCDNEngine.prototype.isComplete = function() {
  return (this.pendingGroups_.length <= 0);
}

var HashedGameCDNGroup = function(engine, name, config) {
  var path = require('path');

  console.log(config)

  this.name = name;
  this.hashSize_ = config.hash_size || 4;

  this.current_ = [];
  this.expand('');

  this.base_url_ = config.base_url;

  this.download_path_ = config.download_path && path.resolve(engine.working_dir_, config.download_path);

  if (!this.download_path_) {
    var matches = /\.(.+?)$/.exec(path.basename(this.base_url_))
    var ext = matches[0];
    var base = util.format('./downloads/%s/{id}%s', name, ext);
    this.download_path_ = path.resolve(engine.working_dir_, base);
  }
};

HashedGameCDNGroup.prototype.expand = function(node) {
  console.log("Expanding: " + node)
  console.log("Expanding: " + typeof(node))

  console.log("Len: " + node.length)
  if (node.length >= this.hashSize_)
    return

  for (var i = 0; i < 16; i += 1) {
    var exp = '' + node + i.toString(16);
    console.log("Pushing: " +  exp +  '('+ typeof(exp)+')')
    this.current_.push(exp)
  }
}

HashedGameCDNGroup.prototype.getNext = function() {
  var next;
  do {
    next = this.current_.unshift();
    console.log("Next: " + next + " ("+ typeof(next) +")")
    this.expand(next.toString());
  } while (next.length < this.hashSize_)

  return next;
}

HashedGameCDNGroup.prototype.next = function() {
  if (this.isComplete()) {
    return false;
  }

  var next = this.getNext();

  var theUrl = this.base_url_.replace('{id}', next);
  var thePath = this.download_path_.replace('{id}', next);

  return new engine_base.SingleDownloadJob(theUrl, thePath);
};

HashedGameCDNGroup.prototype.isComplete = function() {
  return (this.current_.length == 0);
};

module.exports.create = function(config) {
  return new HashedGameCDNEngine(config);
}
