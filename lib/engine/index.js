var engines = module.exports;

engines.create = function(config) {
  var engineClass;

  switch (config.engine) {
    case 'gamecdn':
      engineClass = require('./gamecdn.js')
      break;

    case 'hashed-gamecdn':
      engineClass = require('./hashed-gamecdn.js')
      break;
  }

  if (!engineClass) {
    return false;
  }

  return engineClass.create(config.config);
};
