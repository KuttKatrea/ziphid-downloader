#!/usr/bin/env node
var util = require('util');
var events = require('events');
var mkdirp = require('mkdirp');
var logger = require('../logger')
var assert = require('assert')

function SingleDownloadJob(group, url, path, short_url) {
  this.group_ = group;
  this.url_ = url;
  this.path_ = path;
  this.short_url_ = short_url || url
  this.complete_ = false;
};

util.inherits(SingleDownloadJob, events.EventEmitter);

SingleDownloadJob.prototype.notify = function(text)  {
  logger.log("%s: %s", this.short_url_, text)
}

SingleDownloadJob.prototype.emitComplete = function(status) {
  if (!this.complete_) {
    this.complete_ = true;
    this.emit('complete', status);
  }
}

SingleDownloadJob.prototype.launch = function() {
  var http = require('http');
  var url = require('url');

  var context = this;

  function test() {

    context.notify("Testing");

    var requestData = url.parse(context.url_);
    requestData.method = 'HEAD';
    requestData.headers = {
      "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:12.0) Gecko/20100101 Firefox/12.0 FirePHP/0.7.1"
    };

    if (context.group_.engine_.headers_)
      for (k in context.group_.engine_.headers_)
        requestData.headers[k] = context.group_.engine_.headers_[k]

    var request = http.request(requestData);

    request.on('response', function(response) {
      context.notify("Receiving test response...")

      response.setTimeout(10000, function() {
        context.notify("Response timeout")
        context.emitComplete('error')
        response.close()
      })

      response.on('error', function(err) {
        context.notify(util.format('Error en Test Response: %s', err));
        context.emitComplete('error');
      });

      response.on('end', function() {
        if (this.statusCode == 200) {
          download();
        } else {
          context.notify(util.format('Error %s', this.statusCode));
          context.emitComplete('not_found');
        }
      });

      response.on('close', function() {
        context.notify("Test response closed")
      });

      context.notify("Resuming test response")
      response.resume();
    });

    request.on('error', function(err) {
      context.notify(util.format('Error en Test Request: %s', err));
      context.emitComplete('error');
    });

    request.on('close', function(err) {
      context.notify("Test request closed")
    });

    request.setTimeout(10000, function() {
      context.notify("Request timeout")
      request.abort()
      context.emitComplete('error')
    })

    context.notify("Sending test request...")
    request.end()
  }

  function ensureFileDontExists() {
    var fs = require('fs');
    fs.exists(context.path_, onFileExists)
  }

  function onFileExists(exists) {
    if (exists) {
      context.notify("Already exists")
      context.emitComplete('exists')
    } else {
      ensureDirExists();
    }
  }

  function ensureDirExists() {
    var fs = require('fs')
    var path = require('path')

    var parent = path.dirname(context.path_)

    fs.exists(parent, function(exists) {
      if (exists) {
        test()
      } else {
        mkdirp(parent, function() {
          test()
        })
      }
    });
  }

  function download() {
    var fs = require('fs');

    context.notify("Downloading");

    var requestData = url.parse(context.url_);
    requestData.method = 'GET';
    requestData.headers = {
      "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:12.0) Gecko/20100101 Firefox/12.0 FirePHP/0.7.1",
      "Referer": context.group_.engine_.referer_
    };

    var request = http.request(requestData, function(response) {
      response.on('error', function(err) {
        context.notify(util.format('Error en Download Response: %s', err));
        context.emitComplete('error');
      });

      var file = fs.createWriteStream(context.path_);

      response.pipe(file);

      response.on('end', function() {
        context.notify("Downloaded")
        file.end()
        context.emitComplete('downloaded');
      });

      response.on('close', function() {
        context.notify("Conexión cerrada")
      });

    });

    request.on('error', function(err) {
      context.notify(util.format('Error en Download Request: %s', err));
      context.emitComplete('error');
    });

    request.end();
  }

  ensureFileDontExists();
}

function BaseEngine(config) {
  assert(config, 'config required');
  assert(config.working_dir, 'config.working_dir required');

  this.max_connections_ = ( config && config.max_connections ) || 3;
  this.headers_ = ( config && config.headers ) || {};
  this.working_dir_ = config.working_dir;
  this.jobs_ = {};
  this.nextId_ = 1;
};

util.inherits(BaseEngine, events.EventEmitter);
/*
BaseEngine.prototype.getNextId = function() {
  return this.nextId_ ++;
}

BaseEngine.prototype.isJobsEmpty = function() {
  for (var k in this.jobs_) {
    return false;
  }

  return true;
};
*/
BaseEngine.prototype.next = function() {
  return false;
};

/**BaseEngine.prototype.launch = function() {
  for (var i = 0; i < this.max_connections_; i += 1) {
    this.createNextJob();
  }
};

BaseEngine.prototype.createNextJob = function() {
  var new_id = this.getNextId();
  var next = this.next();

  if (next !== false) {
    next.on('complete', this.onJobComplete_.bind(this, new_id));

    this.jobs_[new_id] = next;

    next.launch();

    return true;
  }

  return false;
}

BaseEngine.prototype.onJobComplete_ = function(id) {
  delete this.jobs_[id];

  if (!this.createNextJob() && this.isJobsEmpty()) {
    this.emit('complete');
  }
};*/

module.exports = {
  BaseEngine: BaseEngine,
  SingleDownloadJob: SingleDownloadJob
}
