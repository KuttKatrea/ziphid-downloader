var util = require('util')
var engine_base = require('./base')
var logger = require('../logger')

function clone(obj, opt_obj2) {
  var cloneObj = extend({}, obj);

  if (opt_obj2)
    extend(cloneObj, opt_obj2);

  return cloneObj;
}

function extend(obj1, obj2) {
  for (var k2 in obj2) {
    if (obj2.hasOwnProperty(k2)) {
      obj1[k2] = obj2[k2];
    }
  }

  return obj1;
}

function lpad(str, length) {
  str = '' + str;
  while (str.length < length)
      str = '0' + str;
  return str;
}

var GameCDNEngine = function(config, working_dir) {
  GameCDNEngine.super_.apply(this, [config, working_dir])
  this.currentGroup_ = 0
  this.groups_ = []
  this.pendingGroups_ = []
  this.alternate_ = config.alternate || false

  for (var k in config.groups) {
    var group_config = clone(config.default || {base_url: ""}, config.groups[k])

    var intervals = group_config.intervals || [ {
      first: (group_config.first !== null ? group_config.first : 1),
      last: (group_config.last !== null ? group_config.last : 1)
    } ]

    delete group_config.intervals;

    for (var i = 0; i < intervals.length; i += 1) {
      var i_config = clone(group_config, intervals[i]);
      var group = new GameCDNGroup(this, k, i_config)

      this.groups_.push(group);
    }

    this.reset();
  }
};

util.inherits(GameCDNEngine, engine_base.BaseEngine);

GameCDNEngine.prototype.reset = function() {
  this.currentGroup_ = 0;
  this.pending_groups_ = [];

  for (var i=0, l=this.groups_.length; i<l; i+=1) {
    this.pending_groups_[i] = this.groups_[i]
  }

  //logger.log("Resseting", this.groups_, this.pending_groups_)
}

GameCDNEngine.prototype.next = function() {
  if (this.isComplete()) {
    return false;
  }

  var group = this.pending_groups_[this.currentGroup_];

  var next = group.next();

  if (group.isComplete()) {
    this.pending_groups_.splice(this.currentGroup_,1)
    logger.log("ZD: Group complete, remaining:", this.pending_groups_.length)
  } else if (this.alternate_) {
    this.currentGroup_ += 1;
    logger.log("ZD: Alternating to group:", this.currentGroup_)
  }

  if (this.currentGroup_ >= this.pending_groups_.length) {
    this.currentGroup_ = 0;
  }

  if (!next && !this.isComplete())
    return this.next();

  return next;
};

GameCDNEngine.prototype.isComplete = function() {
  //logger.log("Is Complete? ", this.pending_groups_)
  return (this.pending_groups_.length <= 0)
}

var GameCDNGroup = function(engine, name, config) {
  this.engine_  = engine;

  var url = require('url');
  var path = require('path');

  //console.log(name, config)

  this.name = name;

  this.first_ = typeof config.first == 'number' ? config.first : 1;
  this.last_ = typeof config.last == 'number' ? config.last : this.first_;

  this.next_ = this.first_;

  this.pad_url_ = config.pad_url || 0;
  this.pad_ = config.pad || this.pad_url_;
  this.base_ = config.base || 10;
  this.ext_change_ = config.ext_change || false;

  this.base_url_ = config.base_url + config.path_url;
  this.short_url_cut_line_ = config.base_url.length;

  this.download_path_ = config.download_path && path.resolve(engine.working_dir_, config.download_path);

  if (!this.download_path_) {
    var url_pieces = url.parse(this.base_url_);
    var matches = /\.(.+?)$/.exec(path.basename(url_pieces.pathname))

    var ext = this.ext_change_ || matches[0];
    var base = util.format('./downloads/%s/{id}%s', name, ext);
    this.download_path_ = path.resolve(engine.working_dir_, base);
  }
};

GameCDNGroup.prototype.next = function() {
  if (this.isComplete()) {
    return false;
  }

  var theUrl = this.base_url_.replace('{id}', lpad(this.next_.toString(this.base_), this.pad_url_));
  var thePath = this.download_path_.replace('{id}', lpad(this.next_.toString(this.base_), this.pad_));

  this.next_ += 1;

  return new engine_base.SingleDownloadJob(this, theUrl, thePath, theUrl.substr(this.short_url_cut_line_));
};

GameCDNGroup.prototype.isComplete = function() {
  return this.next_ > this.last_;
};

module.exports.create = function(config) {
  return new GameCDNEngine(config);
}

