var util = require('util')
var engine_base = require('./base')

var letters = [];
var numbers = [];

function clone(obj, opt_obj2) {
  var cloneObj = extend({}, obj);

  if (opt_obj2)
    extend(cloneObj, opt_obj2);

  return cloneObj;
}

function extend(obj1, obj2) {
  for (var k2 in obj2) {
    if (obj2.hasOwnProperty(k2)) {
      obj1[k2] = obj2[k2];
    }
  }

  return obj1;
}

function lpad(str, length) {
  str = '' + str;
  while (str.length < length)
      str = '0' + str;
  return str;
}

var GameCDNEngine = function(config) {
  GameCDNEngine.super_.apply(this, [config]);

  this.groups_ = {};
  this.pendingGroups_ = [];

  for (var k in config.groups) {
    var group = new GameCDNGroup(this, k, clone(config.default || {}, config.groups[k]))
    this.groups_[k] = group;
    this.pendingGroups_.push(group);
  }

  this.nextGroup_ = 0;
};

util.inherits(GameCDNEngine, engine_base.BaseEngine);

GameCDNEngine.prototype.next = function() {
  if (this.isComplete()) {
    return false;
  }

  this.current_[4] += 1;

  if (this.current_[4] > max_letter) {
    this.current_[4] = min_letter;
    this.current_[3] += 1;
  }

  var group = this.pendingGroups_[this.nextGroup_];

  var next = group.next();

  if (group.isComplete()) {
    this.pendingGroups_.splice(this.nextGroup_, 1);
  } else {
    this.nextGroup_ += 1;
  }

  if (this.nextGroup_ >= this.pendingGroups_.length) {
    this.nextGroup_ = 0;
  }

  return next;
};

GameCDNEngine.prototype.isComplete = function() {
  return (this.pendingGroups_.length <= 0);
}

var GameCDNGroup = function(engine, name, config) {
  this.engine_ = engine;

  var url = require('url');
  var path = require('path');

  this.name = name;
  this.next_ = this.first_ = config.first || 1; //TODO: Validar bien para que 0 sea un valor válido
  this.last_ = config.last || this.first_;
  this.pad_ = config.pad || 4;
  this.pad_url_ = config.pad_url || 0;
  this.base_ = config.base || 10;

  this.base_url_ = config.base_url;

  this.download_path_ = config.download_path && path.resolve(engine.working_dir_, config.download_path);

  if (!this.download_path_) {
    var url_pieces = url.parse(this.base_url_);
    var matches = /\.(.+?)$/.exec(path.basename(url_pieces.pathname))

    var ext = matches[0];
    var base = util.format('./downloads/%s/{id}%s', name, ext);
    this.download_path_ = path.resolve(engine.working_dir_, base);
  }
};

GameCDNGroup.prototype.next = function() {
  if (this.isComplete()) {
    return false;
  }

  var theUrl = this.base_url_.replace('{id}', lpad(this.next_.toString(this.base_), this.pad_url_));
  var thePath = this.download_path_.replace('{id}', lpad(this.next_.toString(this.base_), this.pad_));

  this.next_ += 1;

  return new engine_base.SingleDownloadJob(this, theUrl, thePath);
};

GameCDNGroup.prototype.isComplete = function() {
  return (this.next_ > this.last_);
};

module.exports.create = function(config) {
  return new GameCDNEngine(config);
}

